#Great Hall

# Diagrams

Where possible there is a link to the product that is in use.

```mermaid
---
title: AV Setup
---
graph TD
    Podium --> HDMI_Extender_1_TX[<a href='https://www.amazon.com/gp/product/B07WFLHTKK/'>HDMI_Extender_1_TX</a>] 
    HDMI_Extender_1_TX --> |Cat6| HDMI_Extender_1_RX
    HDMI_Extender_1_RX --> |Input 1| HDMI_Matrix_Input[<a href='https://www.amazon.com/gp/product/B008F29MZ4/'>HDMI Matrix In</a>]
    
    HDMI_Extender_2_TX --> |Cat6| HDMI_Extender_2_RX
    HDMI_Extender_2_RX --> |HDMI 2| Projector[<a href='https://epson.com/For-Home/Projectors/Home-Cinema/PowerLite-Home-Cinema-8350-1080p-3LCD-Projector/p/V11H373120' >Projector</a>] 
    
    HDMI_Cast[<a href='https://www.amazon.com/gp/product/B015UKRNGS/'>Chrome Cast</a>] --> HDMI_Spitter[<a href='https://www.amazon.com/gp/product/B004F9LVXC'>HDMI splitter</a>]
    
    Security_Cameras[Security Cameras] --> |Input 2| HDMI_Matrix_Input
    Streaming_PC --> | Left Monitor <br/> Input 3| HDMI_Matrix_Input
    HDMI_Spitter --> |Input 4| HDMI_Matrix_Input
    
    HDMI_Matrix_Input ==> HDMI_Matrix_Output[HDMI Matrix Out]
    HDMI_Matrix_Output --> |Output A| HDMI_Extender_2_TX
    HDMI_Matrix_Output --> |Output B| Left_Monitor
    HDMI_Matrix_Output --> |RCA outputs -> Tape In| Mixer[<a href='https://www.amazon.com/Mackie-PROFX12V2-12-Channel-Compact-Effects/dp/B00VUU7B7E/ref=sr_1_1'>Mixer</a>]
    Mixer <--> |USB audio| Streaming_PC
```
